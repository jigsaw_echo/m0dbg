#include "LPC11Uxx.h"
#include "core_cm0.h"
#include "core_cmFunc.h"

/* The __cs3_regions array is used by CS3's startup code to initialize
   the contents of memory regions during the C initialization phase.
   __cs3_regions is normally defined in the linker script.  */

typedef unsigned char __cs3_byte_align8 __attribute ((aligned (8)));

struct __cs3_region
{
  unsigned long flags;       /* Flags for this region.  None defined yet.  */
  __cs3_byte_align8 *init;  /* Initial contents of this region.  */
  __cs3_byte_align8 *data;  /* Start address of region.  */
  long init_size;     /* Size of initial data.  */
  long zero_size;     /* Additional size to be zeroed.  */
};

extern const struct __cs3_region __cs3_regions[];

extern void __libc_init_array(void);
extern int main(void);

void __attribute__ ((noreturn))
__cs3_start_c(void)
{
	const struct __cs3_region *reg = __cs3_regions;
	long long *src = (long long *) reg->init;
	long long *dst = (long long *) reg->data;
	unsigned size = reg->init_size;
	unsigned cnt;

	if (src != dst) {
		for (cnt = 0; cnt != size; cnt += sizeof(long long)) {
			*dst++ = *src++;
		}
	} else
		dst = (long long *) ((char *)dst + size);

	size = reg->zero_size;

	for (cnt = 0; cnt != size; cnt += sizeof(long long))
		*dst++ = 0;

	(void) main();

	while (1) ;
}

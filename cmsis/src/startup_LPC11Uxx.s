/*****************************************************************************/
/* startup_LPC11Uxx.s: Startup file for LPC11Uxx device series                 */
/*****************************************************************************/
/* Version: CodeSourcery Sourcery G++ Lite (with CS3)                        */
/*****************************************************************************/

/*
 * Stack Configuration
 * Stack Size (in Bytes) <0x0-0xFFFFFFFF:8>
 */

    .equ    Stack_Size, 0x00000c00
    .section ".stack", "w"
    .align  3
    .globl  __cs3_stack_mem
    .globl  __cs3_stack_size
__cs3_stack_mem:
    .if     Stack_Size
    .space  Stack_Size
    .endif
    .size   __cs3_stack_mem,  . - __cs3_stack_mem
    .set    __cs3_stack_size, . - __cs3_stack_mem

/* Vector Table */

    .section ".cs3.interrupt_vector"
    .globl  __cs3_interrupt_vector_cortex_m
    .type   __cs3_interrupt_vector_cortex_m, %object

__cs3_interrupt_vector_cortex_m:
    .long   __cs3_stack                 /* Top of Stack                 */
    .long   __cs3_reset                 /* Reset Handler                */
    .long   NMI_Handler                 /* NMI Handler                  */
    .long   HardFault_Handler           /* Hard Fault Handler           */
    .long   0                           /* Reserved                     */
    .long   0                           /* Reserved                     */
    .long   0                           /* Reserved                     */
    .long   0                           /* Reserved                     */
    .long   0                           /* Reserved                     */
    .long   0                           /* Reserved                     */
    .long   0                           /* Reserved                     */
    .long   SVC_Handler                 /* SVCall Handler               */
    .long   DebugMon_Handler            /* Debug Monitor Handler        */
    .long   0                           /* Reserved                     */
    .long   PendSV_Handler              /* PendSV Handler               */
    .long   SysTick_Handler             /* SysTick Handler              */

    /* External Interrupts */
    .long   FLEX_INT0_IRQHandler        /* All GPIO pin can be routed to FLEX_INTx */
    .long   FLEX_INT1_IRQHandler
    .long   FLEX_INT2_IRQHandler
    .long   FLEX_INT3_IRQHandler
    .long   FLEX_INT4_IRQHandler
    .long   FLEX_INT5_IRQHandler
    .long   FLEX_INT6_IRQHandler
    .long   FLEX_INT7_IRQHandler
    .long   GINT0_IRQHandler
    .long   GINT1_IRQHandler            /* PIO0 (0:7)                    */
    .long   Default_Handler
    .long   Default_Handler
    .long   Default_Handler
    .long   Default_Handler
    .long   SSP1_IRQHandler             /* SSP1                          */
    .long   I2C_IRQHandler              /* I2C                           */
    .long   TIMER16_0_IRQHandler        /* 16-bit Timer0                 */
    .long   TIMER16_1_IRQHandler        /* 16-bit Timer1                 */
    .long   TIMER32_0_IRQHandler        /* 32-bit Timer0                 */
    .long   TIMER32_1_IRQHandler        /* 32-bit Timer1                 */
    .long   SSP0_IRQHandler             /* SSP0                          */
    .long   UART_IRQHandler             /* UART                          */
    .long   USB_IRQHandler              /* USB IRQ                       */
    .long   USB_FIQHandler              /* USB FIQ                       */
    .long   ADC_IRQHandler              /* A/D Converter                 */
    .long   WDT_IRQHandler              /* Watchdog timer                */
    .long   BOD_IRQHandler              /* Brown Out Detect              */
    .long   FMC_IRQHandler              /* IP2111 Flash Memory Controller */
    .long   Default_Handler
    .long   Default_Handler
    .long   USBWakeup_IRQHandler        /* USB wake up                    */
    .long   Default_Handler

    .size   __cs3_interrupt_vector_cortex_m, . - __cs3_interrupt_vector_cortex_m


    .thumb


/* Reset Handler */

    .section .cs3.reset,"x",%progbits
    .thumb_func
    .globl  __cs3_reset_cortex_m
    .type   __cs3_reset_cortex_m, %function
__cs3_reset_cortex_m:
    .fnstart
    LDR     R0, =SystemInit
    BLX     R0
    LDR     R0, =__cs3_start_c
    BX      R0
    .pool
    .cantunwind
    .fnend
    .size   __cs3_reset_cortex_m,.-__cs3_reset_cortex_m

    .section ".text"

/* Exception Handlers */

    .weak   NMI_Handler
    .type   NMI_Handler, %function
NMI_Handler:
    B       .
    .size   NMI_Handler, . - NMI_Handler

    .weak   HardFault_Handler
    .type   HardFault_Handler, %function
HardFault_Handler:
    B       .
    .size   HardFault_Handler, . - HardFault_Handler

    .weak   SVC_Handler
    .type   SVC_Handler, %function
SVC_Handler:
    B       .
    .size   SVC_Handler, . - SVC_Handler

    .weak   DebugMon_Handler
    .type   DebugMon_Handler, %function
DebugMon_Handler:
    B       .
    .size   DebugMon_Handler, . - DebugMon_Handler

    .weak   PendSV_Handler
    .type   PendSV_Handler, %function
PendSV_Handler:
    B       .
    .size   PendSV_Handler, . - PendSV_Handler

    .weak   SysTick_Handler
    .type   SysTick_Handler, %function
SysTick_Handler:
    B       .
    .size   SysTick_Handler, . - SysTick_Handler


/* IRQ Handlers */

    .globl  Default_Handler
    .type   Default_Handler, %function
Default_Handler:
    B       .
    .size   Default_Handler, . - Default_Handler

    .macro  IRQ handler
    .weak   \handler
    .set    \handler, Default_Handler
    .endm

    IRQ    FLEX_INT0_IRQHandler
    IRQ    FLEX_INT1_IRQHandler
    IRQ    FLEX_INT2_IRQHandler
    IRQ    FLEX_INT3_IRQHandler
    IRQ    FLEX_INT4_IRQHandler
    IRQ    FLEX_INT5_IRQHandler
    IRQ    FLEX_INT6_IRQHandler
    IRQ    FLEX_INT7_IRQHandler
    IRQ    GINT0_IRQHandler
    IRQ    GINT1_IRQHandler
    IRQ    SSP1_IRQHandler
    IRQ    I2C_IRQHandler
    IRQ    TIMER16_0_IRQHandler
    IRQ    TIMER16_1_IRQHandler
    IRQ    TIMER32_0_IRQHandler
    IRQ    TIMER32_1_IRQHandler
    IRQ    SSP0_IRQHandler
    IRQ    UART_IRQHandler
    IRQ    USB_IRQHandler
    IRQ    USB_FIQHandler
    IRQ    ADC_IRQHandler
    IRQ    WDT_IRQHandler
    IRQ    BOD_IRQHandler
    IRQ    FMC_IRQHandler
    IRQ    USBWakeup_IRQHandler

    .end

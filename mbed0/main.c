#include "LPC11Uxx.h"
#include "gdb_stub.h"

static void serial_baud(void)
{
	int i;
	unsigned dll, fdr;

	LPC_SYSCON->UARTCLKDIV = 1;

	LPC_USART->LCR |= 0x80;

	/* this is enough for highest baudrate.
	 * w/o the delay, the mbed-interface is skrewed
     */
	for (i = 0; i < 1000000; i++)
		asm volatile ("nop");

    /* baud 9600 */
    dll = 0xd0;
    fdr = 0x21;

	LPC_USART->DLM = 0;
	LPC_USART->DLL = dll;
	LPC_USART->FDR = fdr;
	LPC_USART->LCR &= ~0x80;
}

static void serial_init(void)
{
	volatile unsigned dummy;

	LPC_SYSCON->SYSAHBCLKCTRL |= 0x1000;

	LPC_IOCON->PIO0_19 &= ~0x7;
	LPC_IOCON->PIO0_18 &= ~0x7;

	LPC_USART->DLM = 0;

	serial_baud();

	LPC_USART->LCR = 0x3; /* 8bits, 1bit stop, non-parity */

	LPC_IOCON->PIO0_19 |= 0x1; /* func */
	LPC_IOCON->PIO0_18 |= 0x1;

	LPC_USART->FCR = 0x7; /* enable and reset TX/RX FIFO */

	dummy = LPC_USART->LSR;
	/* Ensure a clean start, no data in either TX or RX FIFO. */
	while ((LPC_USART->LSR & 0x60) != 0x60) ;

	while (LPC_USART->LSR & 1)
		dummy = LPC_USART->RBR;	/* Dump data from RX FIFO */

}

static void serial_putc(char c)
{
	while (!(LPC_USART->LSR & (1 << 5))) ;
	LPC_USART->THR = c;
}

static char serial_getc(void)
{
	while (!(LPC_USART->LSR & 1)) ;
	return LPC_USART->RBR;
}

int main(void)
{
    char c;
    int len;
    char *r;

	serial_init();

    while (1) {
        c = serial_getc();
        r = gdb_stub_feed(c, &len);
        if (r) {
            while (len) {
                serial_putc(*r++);
                --len;
            }
        }
    }
}

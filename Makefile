CC = arm-none-eabi-gcc
AS = arm-none-eabi-as
LD = arm-none-eabi-gcc
AR = arm-none-eabi-ar

M0DBG_TARGET ?= elli

RM = rm -f

LD_FLAGS = -mcpu=cortex-m0 -mthumb -Wl,--gc-sections
LD_SYS_LIBS = -lm -lc -lgcc

OBJCOPY = arm-none-eabi-objcopy
OBJDUMP = arm-none-eabi-objdump

CFLAGS = -c -Os -g -fno-common -Wall -mcpu=cortex-m0 -mthumb -ffunction-sections -fdata-sections
CFLAGS += -std=gnu99
CFLAGS += -Werror -Wno-unused

IFLAGS = -Iinclude -Iinclude/sys -Icmsis/inc -Idap/inc

vpath %.s cmsis/src
vpath %.c cmsis/src dap/src

# CMSIS
OBJS = core_cm0.o

# dap
OBJS += DAP.o swd_host.o SW_DP.o gdb_stub.o

# main
OBJS += main.o

# LPC11U24

ifeq ($(M0DBG_TARGET), mbed0)
vpath %.c dap/target/LPC11U24 mbed0

OBJS += system_LPC11Uxx.o startup_LPC11Uxx.o startup_LPC11U24.o
CFLAGS += -DLPC11U24
LINKER_SCRIPT = mbed0/LPC11U24.ld

else
#elli

vpath %.c dap/target/stm32f0xx elli

OBJS += startup_stm32f051.o system_stm32f0xx.o
OBJS += stm32f0xx_gpio.o stm32f0xx_rcc.o stm32f0xx_usart.o

IFLAGS += -Ielli/inc
CFLAGS += -DSTM32F051

LINKER_SCRIPT = elli/STM32F051R8_FLASH.ld
endif

CFLAGS += $(IFLAGS)

all: test.bin

.s.o:
	$(AS) -c -mcpu=cortex-m0 -mthumb -o $@ $<

.c.o:
	$(CC) $(CFLAGS) -o $@ $<

test.elf: $(OBJS)
	$(LD) $(LD_FLAGS) -T$(LINKER_SCRIPT) -o $@ $^ $(LD_SYS_LIBS)

test.bin: test.elf
	$(OBJCOPY) -O binary $< $@

.PHONY: all clean

clean:
	$(RM) $(OBJS) test.elf test.bin

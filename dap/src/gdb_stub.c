#include <stdint.h>

#include "swd_host.h"

#ifndef NULL
#define NULL    ((void *) 0)
#endif

#define BUF_SIZE 128


static const char hexdigits[] = "0123456789abcdef";

static char *
reply(char *msg, int *rlen, int no_ack)
{
    static char rbuf[BUF_SIZE];
    unsigned char cksum = 0;
    int i;
    char *p = rbuf;

    if (!no_ack)
        *p++ = '+';

    *p++ = '$';
    for (i = 0; msg[i]; i++) {
        *p++ = msg[i];
        cksum += msg[i] % 256;
    }
    *p++ = '#';
    *p++ = hexdigits[(cksum & 0xf0) >> 4];
    *p++ = hexdigits[cksum & 0xf];
    *p = 0;

    *rlen = no_ack ? i + 4 : i + 5;

    return rbuf;
}

/* wrapper for b2h */
#define _b2h(b, l) b2h((unsigned char *)(b), (l))

/* bytes to hex */
static char *
b2h(unsigned char *bytes, int len)
{
    static char bbuf[BUF_SIZE];
    int i, j;
    unsigned char b;

    for (i = 0, j = 0; i < len; i++, j += 2) {
        b = bytes[i];
        bbuf[j] = hexdigits[(b & 0xf0) >> 4];
        bbuf[j + 1] = hexdigits[b & 0xf];
    }
    bbuf[j] = 0;

    return bbuf;
}

#define _ishex(c) (((c) >= '0' && (c) <= '9') || ((c) >= 'a' && (c) <= 'f'))
#define _c2b(c) (unsigned char)((c) <= '9' ? (c) - '0' : 10 + (c) - 'a')

/* read int */
static unsigned int
h2i(char *hex, int len, char **pnext)
{
    unsigned int r;
    int i, s;
    unsigned char ch;

    i = 0;
    if (len > 8)
        len = 8;

    while (i < len) {
        if (!_ishex(hex[i]))
            break;
        ++i;
    }

    if (pnext)
        *pnext = hex + i;

    r = 0;
    s = 0;
    --i;

    while (i >= 0) {
        ch = hex[i--];
        r |= _c2b(ch) << s;
        s += 4;
    }

    return r;
}

static char *
parse_pkt(char *msg, int len, int *rlen)
{
    static char set_no_ack = 0;
    int no_ack;
    int err = 0;

    no_ack = set_no_ack;

    /* Supported pkts are:
     * ?
     * c
     * D
     * g
     * G
     * k
     * m
     * M
     * p
     * P
     * s
     * z1
     * Z1
     *
     * and General Query Packets:
     * qSupported
     * QStartNoAckMode
     *
     */

#define _REPLY(s) reply((s), rlen, no_ack)

    switch (msg[0]) {
    case '?':
        if (swd_set_target_state(RESET_PROGRAM))
            return _REPLY("T05");
        err = 1;
        break;

    case 'c':
        /* will block until target halt */
        if (swd_set_target_state(RUN_WITH_DEBUG))
            return _REPLY("T05");
        err = 1;
        break;

    case 'D':
        return _REPLY("OK");

    case 'g':
    {
        /* R0 - R12 */
        uint32_t gprs[12] = {0};
        int i;

        for (i = 0; i < 12; i++) {
            if (swd_read_core_register(i, &gprs[i]) == 0) {
                err = 1;
                break;
            }
        }

        if (!err)
            return _REPLY(_b2h(gprs, 12 * 4));
        break;
    }

    /* Kill needs no request */
    case 'k':
        swd_set_target_state(RESET_PROGRAM);
        return NULL;

    case 'm':
    case 'M':
    {
        unsigned int maddr, mlen;
        char *p;
        unsigned char mem[64] = {0};

        maddr = h2i(msg + 1, len - 1, &p);
        len -= (p - msg);
        mlen = h2i(p + 1, len - 1, &p);

        if (msg[0] == 'm') {
            if (swd_read_memory(maddr, mem, mlen))
                return _REPLY(_b2h(mem, mlen));
        } else {
            int i = 0;

            ++p;
            for (i = 0; i < mlen; i++, p += 2)
                mem[i] = (_c2b(p[0]) << 4) | (_c2b(p[1]));
            if (swd_write_memory(maddr, mem, mlen))
                return _REPLY("OK");
        }

        err = 1;
        break;
    }

    case 'p':
    {
        uint32_t regnum = h2i(msg + 1, len - 1, NULL);
        uint32_t reg;

        if (swd_read_core_register(regnum, &reg))
            return _REPLY(_b2h(&reg, 4));
        err = 1;
        break;
    }

    case 'P':
    {
        char *p;
        uint32_t regnum = h2i(msg + 1, len - 1, &p);
        uint32_t reg;

        ++p;
        reg = _c2b(p[0]) << 4;
        reg |= _c2b(p[1]);
        reg |= _c2b(p[2]) << 12;
        reg |= _c2b(p[3]) << 8;
        reg |= _c2b(p[4]) << 20;
        reg |= _c2b(p[5]) << 16;
        reg |= _c2b(p[6]) << 28;
        reg |= _c2b(p[7]) << 24;

        if (swd_write_core_register(regnum, reg))
            return _REPLY("OK");
        err = 1;
        break;
    }

    case 'q':
        /* qSupported */
        if (len > 10 && msg[1] == 'S' && msg[2] == 'u' && msg[3] == 'p')
            return _REPLY("QStartNoAckMode+");

        break;

    case 'Q':
        /* QStartNoAckMode */
        if (len == 15 && msg[1] == 'S' && msg[2] == 't' && msg[3] == 'a' &&
            msg[4] == 'r' && msg[5] == 't') {
            set_no_ack = 1;
            return _REPLY("OK");
        }
        break;

    case 's':
        if (swd_set_target_state(STEP))
            return _REPLY("T05");
        err = 1;
        break;

    case 'Z':
    {
        uint32_t iaddr;

        if (msg[1] != '1')
            break;

        iaddr = (uint32_t) h2i(msg + 3, len - 3, NULL);
        if (swd_set_breakpoint(iaddr))
            return _REPLY("OK");

        err = 1;
        break;
    }

    case 'z':
    {
        uint32_t iaddr;

        if (msg[1] != '1')
            break;

        iaddr = (uint32_t) h2i(msg + 3, len - 3, NULL);
        if (swd_unset_breakpoint(iaddr))
            return _REPLY("OK");
        err = 1;
        break;
    }


    default:
        break;
    }

    /* empty pkt being innocent */
    if (err)
        return _REPLY("E01");
    else
        return _REPLY("");
#undef _REPLY
}

char *
gdb_stub_feed(char c, int *rlen)
{
    static char ubuf[BUF_SIZE];
    static char pkt_stat = 0; /* 0 - init, 1 - started, 2 - ended */
    static char cksum_cnt = 0;
    static short offset = 0;

    char *result = NULL;

    switch (c) {
    case '$': pkt_stat = 1;
    case '+':   /* ignore Ack */
    case '-':   /* we have nothing to retransmit */
        return NULL;

    case '#':   /* need to wait for 2 more digits the checksum */
        pkt_stat = 2;
        return NULL;

    default:
        break;
    }

    if (pkt_stat == 0)
        return NULL;

    if (pkt_stat == 1) {
        ubuf[offset++] = c;
        return NULL;
    }

    cksum_cnt++;
    if (cksum_cnt == 2) {
        result = parse_pkt(ubuf, offset, rlen);
        offset = 0;
        pkt_stat = 0;
        cksum_cnt = 0;

        return result;
    }

    return NULL;
}

